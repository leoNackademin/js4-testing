before(function() {
    cy.clearLocalStorage();
});

describe("login test", function() {
    it("Visits my local host and logs in", function() {
        cy.visit("/");

        cy.get('input[name="email"')
        .type("name@domain.com")
        .should('have.value', "name@domain.com");

        cy.get('input[name="password"')
        .type("ASDasd123")
        .should('have.value', 'ASDasd123');

        cy.server();
        cy.route('POST', '**/verifyPassword**').as('verifyPassword');
        cy.route('POST', '**/getAccountInfo**').as('getAccountInfo');

        cy.get('input[type="submit"]').click();

        cy.wait('@verifyPassword');
        cy.wait('@getAccountInfo');

        cy.get('.user').should('contain', 'name@domain.com');
    })
})