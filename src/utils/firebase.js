import * as firebase from 'firebase/app';
import 'firebase/auth';

const config = {
    apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
    authDomain: "js4-testing.firebaseapp.com",
    databaseURL: "https://js4-testing.firebaseio.com",
    projectId: "js4-testing",
    storageBucket: "js4-testing.appspot.com",
    messagingSenderId: "671987891180"
}

class Firebase {
    constructor() {
        firebase.initializeApp(config);

        this.auth = firebase.auth();
    }

    signIn = (email, password) => this.auth.signInWithEmailAndPassword(email, password);
}

const api = new Firebase();

export default api;